﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public interface IAuthorization
    {
        User SignUp(string login, string password);
        bool SignIn(string login, string password);
    }
}
