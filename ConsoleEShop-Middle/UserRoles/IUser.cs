﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public interface IUser
    {
        public int Id { get; }
        public string Login { get; set; }
        public string Password { get; set; }

    }
}
