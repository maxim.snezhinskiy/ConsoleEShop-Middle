﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public enum OrderStatus
    {
        New,
        CanceledByAdmin,
        CanceledByUser,
        ReceivedPayment,
        Sent,
        Completed
    }
}
