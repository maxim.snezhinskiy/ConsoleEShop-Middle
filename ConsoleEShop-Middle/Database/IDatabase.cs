﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public interface IDatabase<T, in K>
    {
        public bool Add(T data);
        public T Find(K data);
        public int Count { get;}
    }
}
