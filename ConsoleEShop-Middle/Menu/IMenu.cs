﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public interface IMenu
    {
        public EventHandler LoginNotify { get; set; }
        public EventHandler LogOutNotify { get; set; }
        public void Execute();
    }
}
