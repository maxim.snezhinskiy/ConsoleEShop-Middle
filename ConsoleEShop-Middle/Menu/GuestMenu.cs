﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public class GuestMenu : ClientMenu, IAuthorization
    {

        public GuestMenu() 
        {
            Commands = new Dictionary<int, Action>()
            {
                { 1, PrintPoductList },
                { 2, SearchProduct },
                { 3, CreateAccount },
                { 4,  LogIn}
            };
        }
        public override void PrintMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-- Menu");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("1. Show all products");
            Console.WriteLine("2. Search product by name");
            Console.WriteLine("3. Sign Up");
            Console.WriteLine("4. Sign In");
        }

        public override void GetUserInput(string data) 
        {
            int parsed=0;
            int.TryParse(data,out parsed);
            if (Commands.ContainsKey(parsed))
            {
                Commands[parsed].Invoke();
            }
        }
        public override void Execute() 
        {
            PrintMenu();
            Console.Write("Your choice: ");
            GetUserInput(Console.ReadLine());
        }

        public override void PrintPoductList()
        {
            base.PrintPoductList();
            Console.WriteLine("Press any key back to menu...");
            Console.ReadKey();
        }

        public void CreateAccount() 
        {
            Console.Clear();
            Console.WriteLine("--Creation of new account --");
            Console.Write("Enter login: ");
            var login = Console.ReadLine();
            if (Database.Users.UserExist(login))
            {
                Console.WriteLine("User with such login is already exist!");
                Console.ReadKey();
                return;
            }
            Console.Write("Enter password: ");
            var password = Console.ReadLine();
            var newUser = SignUp(login, password);
            Database.Users.Add(newUser);
            LoginNotify?.Invoke(newUser, new EventArgs());
            Console.ReadKey();

        }

        public void LogIn() 
        {
            Console.WriteLine("-- Log In --");
            Console.Write("Enter login: ");
            var login = Console.ReadLine();
            Console.Write("Enter password: ");
            var password = Console.ReadLine();

            if (!SignIn(login, password))
            {
                Console.WriteLine("Wrong login or password!");
                Console.ReadKey();
                return;
            }
            IUser userToFind = new User(login, password);
            var user = Database.Users.Find(userToFind);
            LoginNotify?.Invoke(user, new EventArgs());
        }

        public bool SignIn(string login, string password)
        {
            return Database.Users.UserExist(login, password);
        }

        public User SignUp(string login, string password)
        {
            return new RegisteredUser(login, password);
        }
    }
}
